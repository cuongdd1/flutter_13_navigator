import 'package:flutter/material.dart';

class CollectionBasic extends StatelessWidget {
  static const routeName = 'CollectionBasic';
  const CollectionBasic({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Collection'),
      ),
    );
  }
}
