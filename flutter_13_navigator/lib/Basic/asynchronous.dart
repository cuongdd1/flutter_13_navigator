import 'dart:async';

import 'package:flutter/material.dart';

class AsynchronousScreen extends StatefulWidget {
  static const routeName = 'AsynchronousScreen';
  const AsynchronousScreen({Key? key}) : super(key: key);

  @override
  _AsynchronousScreenState createState() => _AsynchronousScreenState();
}

class _AsynchronousScreenState extends State<AsynchronousScreen> {
  int resultCallBack = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Asynchronous'),
      ),
      // body: const MyProductionPage(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            ElevatedButton(
              onPressed: () {
                getAge().then((value) {
                  resultCallBack += value;
                  setState(() {});
                });
              },
              child: const Text('clicked'),
            ),
            Text(
              '$resultCallBack',
            ),
          ],
        ),
      ),
    );
  }
}

Future<int> getAge() {
  final completer = Completer<int>();
  Future.delayed(const Duration(seconds: 2), () {
    completer.complete(100);
  });
  return completer.future;
}

class MyProductionPage extends StatefulWidget {
  const MyProductionPage({Key? key}) : super(key: key);

  @override
  _MyProductionPageState createState() => _MyProductionPageState();
}

class _MyProductionPageState extends State<MyProductionPage> {
  final List<ProductionModel> _productionList = [
    ProductionModel(
      'iPhone',
      'iPhone is the stylist phone ever',
      1000,
      'iphone.jpg',
    ),
    ProductionModel(
      'Pixel',
      'Pixel is the most featureful phone ever',
      800,
      'pixel.png',
    ),
    ProductionModel(
      'Laptop',
      'Laptop is most productive development tool',
      2000,
      'laptop.jpg',
    ),
    ProductionModel(
      'Tablet',
      'Tablet is the most useful device ever for meeting',
      1500,
      'tablet.jpg',
    ),
    ProductionModel(
      "Pendrive",
      "Pendrive is useful storage medium",
      100,
      "pendrive.jpg",
    ),
    ProductionModel(
      "Floppy Drive",
      "Floppy drive is useful rescue storage medium",
      20,
      "floppydisk.jpg",
    ),
  ];

  Widget _buildListProduct(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      padding: const EdgeInsets.fromLTRB(2.0, 10.0, 2.0, 10.0),
      itemCount: _productionList.length,
      itemBuilder: (context, index) => ProductBox(
        name: _productionList[index].name,
        description: _productionList[index].description,
        price: _productionList[index].price,
        image: _productionList[index].image,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildListProduct(context);
  }
}

class RatingBox extends StatefulWidget {
  const RatingBox({Key? key}) : super(key: key);

  @override
  _RatingBoxState createState() => _RatingBoxState();
}

class _RatingBoxState extends State<RatingBox> {
  int _rating = 0;
  void _setRatingAsOne() {
    setState(() {
      _rating = 1;
    });
  }

  void _setRatingAsTwo() {
    setState(() {
      _rating = 2;
    });
  }

  void _setRatingAsThree() {
    setState(() {
      _rating = 3;
    });
  }

  @override
  Widget build(BuildContext context) {
    double _size = 20;
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 1
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsOne,
            iconSize: _size,
          ),
        ),
        Container(
          padding: const EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 2
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsTwo,
            iconSize: _size,
          ),
        ),
        Container(
          padding: const EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 3
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsThree,
            iconSize: _size,
          ),
        ),
      ],
    );
  }
}

class ProductBox extends StatelessWidget {
  final String name;
  final String description;
  final int price;
  final String image;

  const ProductBox(
      {Key? key,
      this.name = 'default name',
      this.description = 'description',
      this.price = 9,
      this.image = 'image'})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      height: 200,
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Image.asset(
              "assets/images/" + image,
              width: 64,
              height: 64,
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      name,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      description,
                    ),
                    Text("Price: " + price.toString()),
                    const RatingBox(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ProductionModel {
  late String name;
  late String description;
  late int price;
  late String image;

  ProductionModel(this.name, this.description, this.price, this.image);
}
