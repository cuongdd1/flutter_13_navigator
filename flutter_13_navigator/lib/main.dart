import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/Basic/asynchronous.dart';
import 'package:flutter_13_navigator/Basic/collection.dart';
import 'package:flutter_13_navigator/CookBook/Animation/animate_page_route_transition.dart';
import 'package:flutter_13_navigator/CookBook/Animation/animation_screen.dart';
import 'package:flutter_13_navigator/CookBook/Effects/download_button.dart';
import 'package:flutter_13_navigator/CookBook/Effects/effects_group.dart';
import 'package:flutter_13_navigator/CookBook/Images/images_from_internet.dart';
import 'package:flutter_13_navigator/CookBook/Images/images_screen.dart';
import 'package:flutter_13_navigator/CookBook/Lists/demo_search_in_list.dart';
import 'package:flutter_13_navigator/CookBook/Lists/different_types_items.dart';
import 'package:flutter_13_navigator/CookBook/Lists/grid_list.dart';
import 'package:flutter_13_navigator/CookBook/Lists/horizontal_list.dart';
import 'package:flutter_13_navigator/CookBook/Lists/lists_screen.dart';
import 'package:flutter_13_navigator/CookBook/Navigation/Send_data_to_screen/send_data_screen.dart';

import 'package:flutter_13_navigator/CookBook/Navigation/navigation_screen.dart';
import 'package:flutter_13_navigator/CookBook/Networking/SimpleDemo/Screens/detail_cocktail.dart';
import 'package:flutter_13_navigator/CookBook/Networking/SimpleDemo/Screens/list_cocktail.dart';
import 'package:flutter_13_navigator/CookBook/Networking/networking_screen.dart';
import 'package:flutter_13_navigator/CookBook/cookbook_screen.dart';
import 'package:flutter_13_navigator/CookBook/null_screen.dart';

import 'CookBook/Navigation/Back data from screen/return_data.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // initialRoute: 'homeScreen',
      initialRoute: 'MyHomeScreen',
      routes: {
        'homeScreen': (context) => const HomeScreen(),
        MyHomeScreen.routeName: (context) => const MyHomeScreen(),
        RegisterScreen.routeName: (context) => const RegisterScreen(),
        LoginScreen.routeName: (context) => const LoginScreen(),
        CookBookScreen.routeName: (context) => const CookBookScreen(),
        HorizontalList.routeName: (context) => const HorizontalList(),
        GridList.routeName: (context) => const GridList(),
        EffectsWidget.routeName: (context) => const EffectsWidget(),
        ExampleCupertinoDownloadButton.routeName: (context) =>
            const ExampleCupertinoDownloadButton(),
        AnimationScreen.routeName: (context) => const AnimationScreen(),
        AnimateTransitionScreen.routeName: (context) =>
            const AnimateTransitionScreen(),
        ListsScreen.routeName: (context) => const ListsScreen(),
        NullScreen.routeName: (context) => const NullScreen(),
        ImagesScreen.routeName: (context) => const ImagesScreen(),
        ImagesFromInternetScreen.routeName: (context) =>
            const ImagesFromInternetScreen(),
        DifferentTypesItemsScreen.routeName: (context) =>
            const DifferentTypesItemsScreen(),
        CollectionBasic.routeName: (context) => const CollectionBasic(),
        AsynchronousScreen.routeName: (context) => const AsynchronousScreen(),
        Networking.routeName: (context) => const Networking(),
        ListCocktail.routeName: (context) => const ListCocktail(),
        DetailCocktail.routeName: (context) => const DetailCocktail(),
        NavigationScreen.routeName: (context) => const NavigationScreen(),
        HomeDataScreen.routeName: (context) => const HomeDataScreen(),
        SendDataScreen.routeName: (context) => const SendDataScreen(),
        FindWordsScreen.routeName: (context) => const FindWordsScreen(),
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomeScreen(),
      // debugShowCheckedModeBanner: false,
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            BuildInterface().createElevatedButton(
                context,
                'Login',
                Icons.login,
                () => BuildInterface()
                    .pushNamedToScreen(context, ScreenType.login)),
            BuildInterface().createElevatedButton(
                context,
                'Register',
                Icons.app_registration,
                () => BuildInterface()
                    .pushNamedToScreen(context, ScreenType.register)),
            BuildInterface().createElevatedButton(
                context,
                'Cookbook',
                Icons.book,
                () => BuildInterface()
                    .pushNamedToScreen(context, ScreenType.cookbook)),
            BuildInterface().createElevatedButton(
                context,
                'Basic',
                Icons.sim_card,
                () => BuildInterface()
                    .pushNamedToScreen(context, ScreenType.basic)),
          ],
        ),
      ),
    );
  }
}

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  static const routeName = 'registerScreen';

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  UserArgument arguments = UserArgument(
    '',
    '',
  );
  String username = '';
  String password = '';

  void textDidChanged(String text, TypeTextFormField type) {
    if (type == TypeTextFormField.email) {
      username = text;
    } else {
      password = text;
    }
    arguments = UserArgument(
      username,
      password,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            BuildInterface().createTextFormField(
                TypeTextFormField.email,
                Icons.email,
                'example@gmail.com',
                'Email',
                (text, type) => textDidChanged(text, type)),
            BuildInterface().createTextFormField(
                TypeTextFormField.password,
                Icons.email,
                '123@Qweasd',
                'Password',
                (text, type) => textDidChanged(text, type)),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                BuildInterface().createElevatedButton(
                    context,
                    'Cancel',
                    Icons.cancel,
                    () => BuildInterface().popPreviousScreen(context)),
                BuildInterface().createElevatedButton(
                    context,
                    'Login',
                    Icons.login,
                    () => BuildInterface().pushNamedToScreen(
                        context, ScreenType.login,
                        user: arguments)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  static const routeName = 'loginScreen';

  void _getText(String text) {}

  Widget _cancelElevatedButton(BuildContext context) {
    return ElevatedButton.icon(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: const Icon(Icons.cancel),
      label: const Text(
        'Cancel',
      ),
    );
  }

  Widget _loginElevatedButton(BuildContext context) {
    return ElevatedButton.icon(
      onPressed: () {},
      icon: const Icon(Icons.login),
      label: const Text('Login'),
    );
  }

  Widget _emailTextFormField(String? name) {
    return TextFormField(
      initialValue: name,
      onChanged: (value) => _getText(value),
      decoration: const InputDecoration(
        icon: Icon(Icons.email),
        hintText: 'example@gmail.com',
        label: Text('Email'),
      ),
    );
  }

  Widget _passwordTextFormField(String? password) {
    return TextFormField(
      initialValue: password,
      obscureText: true,
      onChanged: (value) => _getText(value),
      decoration: const InputDecoration(
        icon: Icon(Icons.lock),
        hintText: 'pofjjfAj2oo@',
        label: Text('Password'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as UserArgument;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              _emailTextFormField(args.name),
              _passwordTextFormField(args.password),
              const SizedBox(
                height: 16.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  _cancelElevatedButton(context),
                  _loginElevatedButton(context),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class UserArgument {
  final String name;
  final String password;

  UserArgument(this.name, this.password);
}

enum ScreenType {
  login,
  register,
  cookbook,
  basic,
  cancel,
}

enum TypeTextFormField {
  email,
  password,
}

class BuildInterface {
  Widget createElevatedButton(
      BuildContext context, String title, IconData icon, Function()? onPress) {
    return ElevatedButton.icon(
      onPressed: onPress,
      icon: Icon(icon),
      label: Text(
        title,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 24,
        ),
      ),
    );
  }

  void popPreviousScreen(BuildContext context) {
    Navigator.pop(context);
  }

  void pushNamedToScreen(BuildContext context, ScreenType screen,
      {UserArgument? user}) {
    String routeName = '';
    Object? arguments;

    switch (screen) {
      case ScreenType.login:
        routeName = LoginScreen.routeName;
        arguments = user;
        break;
      case ScreenType.cookbook:
        routeName = CookBookScreen.routeName;
        arguments = 'CookBook';
        break;
      case ScreenType.register:
        routeName = RegisterScreen.routeName;
        break;
      case ScreenType.basic:
        routeName = CollectionBasic.routeName;
        break;
      default:
        break;
    }
    Navigator.pushNamed(context, routeName, arguments: arguments);
  }

  void pushNamedNextToScreen(
      BuildContext context, int index, List<IdentifierScreenModel> items) {
    final routeName = items[index].routeName;
    Object arguments;
    final title = items[index].title;
    if (title == 'Login') {
      arguments = UserArgument(
        'cuongdd',
        '1234',
      );
    } else {
      arguments = title;
    }
    Navigator.pushNamed(context, routeName, arguments: arguments);
  }

  Widget createTextFormField(
      TypeTextFormField type,
      IconData icon,
      String hintText,
      String labelText,
      Function(String text, TypeTextFormField type) textChanged) {
    return TextFormField(
      onChanged: (value) => textChanged(value, type),
      decoration: InputDecoration(
        icon: Icon(icon),
        hintText: hintText,
        label: Text(labelText),
      ),
      obscureText: type == TypeTextFormField.password,
    );
  }
}

class MyHomeScreen extends StatefulWidget {
  static const routeName = 'MyHomeScreen';
  const MyHomeScreen({Key? key}) : super(key: key);

  @override
  _MyHomeScreenState createState() => _MyHomeScreenState();
}

class _MyHomeScreenState extends State<MyHomeScreen> {
  final List<IdentifierScreenModel> _listScreens = [
    const IdentifierScreenModel('Login', LoginScreen.routeName),
    const IdentifierScreenModel('Register', RegisterScreen.routeName),
    const IdentifierScreenModel('CookBook', CookBookScreen.routeName),
    const IdentifierScreenModel('Collection Basic', CollectionBasic.routeName),
    const IdentifierScreenModel(
        'FUTURE, ASYNC, AWAIT, THEN', AsynchronousScreen.routeName),
  ];

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _listScreens.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blueAccent,
        shadowColor: Colors.blueGrey,
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        child: _cellForRowsAt(context, index),
      ),
    );
  }

  Widget _cellForRowsAt(BuildContext context, int index) {
    return ListTile(
      title: Text(
        _listScreens[index].title,
        style: const TextStyle(color: Colors.white),
      ),
      onTap: () =>
          BuildInterface().pushNamedNextToScreen(context, index, _listScreens),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: _buildListView(context),
      ),
    );
  }
}
