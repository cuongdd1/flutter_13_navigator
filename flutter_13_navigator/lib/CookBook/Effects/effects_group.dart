import 'package:flutter/material.dart';
import '../cookbook_screen.dart';

class EffectsWidget extends StatefulWidget {
  static const routeName = 'Effects';
  const EffectsWidget({Key? key}) : super(key: key);

  @override
  _EffectsWidgetState createState() => _EffectsWidgetState();
}

class _EffectsWidgetState extends State<EffectsWidget> {
  final List<IdentifierScreenModel> _effectsList = [
    const IdentifierScreenModel('Create a download button', 'ExampleCupertinoDownloadButton'),
    const IdentifierScreenModel('Create a nested navigation flow', ''),
    const IdentifierScreenModel('Create a photo filter carousel', ''),
    const IdentifierScreenModel('Create a scrolling parallax effect', ''),
    const IdentifierScreenModel('Create a shimmer loading effect', ''),
    const IdentifierScreenModel('Create a staggered menu animation', ''),
    const IdentifierScreenModel('Create a typing indicator', ''),
    const IdentifierScreenModel('Create an expandable FAB', ''),
    const IdentifierScreenModel('Create gradient chat bubbles', ''),
    const IdentifierScreenModel('Drag a UI element', ''),
  ];

  void _pushNamedToScreen(BuildContext context, int index) {
    final routeName = _effectsList[index].routeName;
    final title = _effectsList[index].title;
    Navigator.pushNamed(context, routeName, arguments: title);
  }

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _effectsList.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blue,
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        child: _cellForRowsAt(context, index),
      ),
    );
  }

  Widget _cellForRowsAt(BuildContext context, int index) {
    return ListTile(
      title: Text(
        _effectsList[index].title,
        style: const TextStyle(color: Colors.white),
      ),
      onTap: () {
        _pushNamedToScreen(context, index);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Effects'),
      ),
      body: Container(
        padding: const EdgeInsetsDirectional.all(16),
        child: _buildListView(context),
      ),
    );
  }
}


