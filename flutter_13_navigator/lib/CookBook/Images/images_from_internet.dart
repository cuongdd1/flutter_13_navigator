//hiển thị image từ internet. Dùng thư viện transparent_image
import 'package:flutter/material.dart';

class ImagesFromInternetScreen extends StatelessWidget {
  static const routeName = 'ImagesFromInternetScreen';
  const ImagesFromInternetScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Image.network(
          'https://www.thecocktaildb.com/images/media/drink/metwgh1606770327.jpg',
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
      ),
    );
  }
}
