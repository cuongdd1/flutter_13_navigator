import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/CookBook/cookbook_screen.dart';

class ImagesScreen extends StatefulWidget {
  static const routeName = 'ImagesScreen';
  const ImagesScreen({Key? key}) : super(key: key);

  @override
  _ImagesScreenState createState() => _ImagesScreenState();
}

class _ImagesScreenState extends State<ImagesScreen> {
  final List<IdentifierScreenModel> _listScreen = [
    const IdentifierScreenModel('Display images from the internet', 'ImagesFromInternetScreen'),
    const IdentifierScreenModel('Fade in images with a placeholder', ''),
    const IdentifierScreenModel('Work with cached images', ''),
  ];

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _listScreen.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blueAccent,
        shadowColor: Colors.blueGrey,
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        child: cellForRowsAt(context, index),
      ),
    );
  }

  Widget cellForRowsAt(context, index) {
    return ListTile(
      title: Text(
        _listScreen[index].title,
        style: const TextStyle(color: Colors.white),
      ),
      onTap: () {
        _pushNamedToScreen(context, index);
      },
    );
  }

  void _pushNamedToScreen(BuildContext context, int index) {
    final routeName = _listScreen[index].routeName;
    final title = _listScreen[index].title;
    Navigator.pushNamed(context, routeName, arguments: title);
  }

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: _buildListView(context),
      ),
    );
  }
}
