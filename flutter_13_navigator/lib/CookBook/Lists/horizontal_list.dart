import 'package:flutter/material.dart';

class HorizontalList extends StatelessWidget {
  static const routeName = 'HorizontalList';

  const HorizontalList({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 16),
        height: 200,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            Container(
              width: 200,
              color: Colors.blue,
            ),
            Container(
              width: 200,
              color: Colors.red,
            ),
            Container(
              width: 200,
              color: Colors.purple,
            ),
            Container(
              width: 200,
              color: Colors.yellow,
            ),
            Container(
              width: 200,
              color: Colors.pink,
            ),
            Container(
              width: 200,
              color: Colors.orange,
            ),
          ],
        ),
      ),
    );
  }
}