import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class FindWordsScreen extends StatefulWidget {
  static const routeName = 'FindWordsScreen';
  const FindWordsScreen({Key? key}) : super(key: key);

  @override
  _FindWordsScreenState createState() => _FindWordsScreenState();
}

class _FindWordsScreenState extends State<FindWordsScreen> {
  List<WordPair> _sourceWords = <WordPair>[];
  List<WordPair> _foundWords = <WordPair>[];
  int get _totalList => _foundWords.length;

  @override
  void initState() {
    getDataSource();
    super.initState();
  }

  void getDataSource() {
    _sourceWords = generateWordPairs().take(500).toList();
    _foundWords = _sourceWords;
  }

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
        itemCount: _foundWords.length,
        itemBuilder: (context, index) => _buildCell(context, index));
  }

  Widget _buildCell(BuildContext context, int index) {
    return ListTile(
      leading: Text((index + 1).toString()),
      title: Text(_foundWords[index].first),
      subtitle: Text(_foundWords[index].second),
      trailing: const Icon(Icons.arrow_forward_ios),
    );
  }

  void _runFilter(String value) {
    final keyWord = value.toLowerCase();
    _foundWords = _sourceWords.where((wordPair) {
      return wordPair.first.contains(keyWord) ||
          wordPair.second.contains(keyWord);
    }).toList();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final String title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
        ),
      ),
      body: Column(
        children: <Widget>[
          TextFormField(
            onChanged: (value) => _runFilter(value),
          ),
          Expanded(
              child: _foundWords.isEmpty
                  ? const Text(
                      'word not found',
                      style: TextStyle(fontSize: 32),
                    )
                  : _buildListView(context)),
          Text(
            _totalList.toString(),
          ),
        ],
      ),
    );
  }
}
