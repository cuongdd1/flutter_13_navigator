//hiển thị list với các kiểu data khác nhau của items
import 'package:flutter/material.dart';

class DifferentTypesItemsScreen extends StatefulWidget {
  static const routeName = 'DifferentTypesItemsScreen';
  const DifferentTypesItemsScreen({Key? key}) : super(key: key);

  @override
  _DifferentTypesItemsScreenState createState() =>
      _DifferentTypesItemsScreenState();
}

class _DifferentTypesItemsScreenState extends State<DifferentTypesItemsScreen> {
  List<ListItem> items = [];

  @override
  void initState() {
    getDataSources();
    super.initState();
  }

  void getDataSources() {
    items = List<ListItem>.generate(
      1000,
      (i) => i % 6 == 0
          ? HeadingItem('Heading $i')
          : MessageItem('Sender $i', 'Message body $i'),
    );

    final List<String> names = ['a', 'b', 'c', 'd', 'b'];
    names.any((element) => element == 'da');
    names.every((element) => element == 'a'); //all of item == a => true
    names.map((e) => 'add item $e');
  }

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          final item = items[index];
          return ListTile(
            title: item.buildTitle(context),
            subtitle: item.buildSubTitle(context),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: _buildListView(context),
    );
  }
}

abstract class ListItem {
  Widget buildTitle(BuildContext context);
  Widget buildSubTitle(BuildContext context);
}

class HeadingItem implements ListItem {
  final String heading;
  HeadingItem(this.heading);

  @override
  Widget buildTitle(BuildContext context) {
    return Text(
      heading,
      style: Theme.of(context).textTheme.headline5,
    );
  }

  @override
  Widget buildSubTitle(BuildContext context) => const SizedBox.shrink();
}

class MessageItem implements ListItem {
  final String sender;
  final String body;

  MessageItem(this.sender, this.body);

  @override
  Widget buildTitle(BuildContext context) => Text(sender);
  @override
  Widget buildSubTitle(BuildContext context) => Text(body);
}
