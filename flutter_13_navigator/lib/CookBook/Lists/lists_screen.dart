import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/CookBook/cookbook_screen.dart';

class ListsScreen extends StatefulWidget {
  static const routeName = 'ListsScreen';
  const ListsScreen({Key? key}) : super(key: key);

  @override
  _ListsScreenState createState() => _ListsScreenState();
}

class _ListsScreenState extends State<ListsScreen> {
  final List<IdentifierScreenModel> _listScreen = [
    const IdentifierScreenModel('Create a grid list', 'GridList'),
    const IdentifierScreenModel('Create a horizontal list', 'HorizontalList'),
    const IdentifierScreenModel(
        'Create lists with different types of items', 'DifferentTypesItemsScreen'),
    const IdentifierScreenModel('Place a floating app bar above a list', ''),
    const IdentifierScreenModel('Use lists', ''),
    const IdentifierScreenModel('Work with long lists', ''),
    const IdentifierScreenModel('Find Words Screen', 'FindWordsScreen'),
  ];

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _listScreen.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blueAccent,
        shadowColor: Colors.blueGrey,
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        child: cellForRowsAt(context, index),
      ),
    );
  }

  Widget cellForRowsAt(BuildContext context, int index) {
    return ListTile(
      title: Text(
        _listScreen[index].title,
        style: const TextStyle(color: Colors.white),
      ),
      onTap: () {
        pushNamedToScreen(context, index);
      },
    );
  }

  void pushNamedToScreen(BuildContext context, int index) {
    final routeName = _listScreen[index].routeName;
    final title = _listScreen[index].title;
    Navigator.pushNamed(context, routeName, arguments: title);
  }

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: _buildListView(context),
      ),
    );
  }
}
