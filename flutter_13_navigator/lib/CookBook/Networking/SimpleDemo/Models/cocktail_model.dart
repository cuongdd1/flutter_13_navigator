class CocktailModel {
  final String? idDrink;
  final String? strDrink;
  final String? strInstructions;
  final String? strGlass;

  final String? strDrinkThumb;

  const CocktailModel({
    required this.idDrink,
    required this.strDrink,
    required this.strInstructions,
    required this.strGlass,
    required this.strDrinkThumb,
  });

  factory CocktailModel.fromJson(Map<String, dynamic> json) {
    return CocktailModel(
      idDrink: json['idDrink'] as String?,
      strDrink: json['strDrink'] as String?,
      strInstructions: json['strInstructions'] as String?,
      strGlass: json['strGlass'] as String?,
      strDrinkThumb: json['strDrinkThumb'] as String?,
    );
  }
}

class DataModel {
  List<CocktailModel>? drinks;
  DataModel(this.drinks);
  factory DataModel.fromJson(Map<String, dynamic> json) {
    return DataModel(
      json['drinks'] as List<CocktailModel>?,
    );
  }
}
