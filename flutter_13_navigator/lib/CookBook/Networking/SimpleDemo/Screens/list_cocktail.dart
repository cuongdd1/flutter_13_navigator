import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/CookBook/Networking/SimpleDemo/Models/cocktail_model.dart';
import 'package:flutter_13_navigator/CookBook/Networking/SimpleDemo/Screens/detail_cocktail.dart';
import 'package:http/http.dart' as http;
import 'package:shimmer/shimmer.dart';

void printMessage(String message) {
  if (kDebugMode) {
    print(message);
  }
}

Future<List<CocktailModel>> fetchCocktails(http.Client client) async {
  final response = await client.get(
      Uri.parse('https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a'));

  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parseCocktails, response.body);
}

// A function that converts a response body into a List<Photo>.
List<CocktailModel> parseCocktails(String responseBody) {
  final parsed = jsonDecode(responseBody);
  printMessage('parsed:\n$parsed');
  printMessage('parsed:\n${parsed['drinks']}');

  return parsed['drinks']
      .map<CocktailModel>((json) => CocktailModel.fromJson(json))
      .toList();
}

class ListCocktail extends StatelessWidget {
  static const routeName = 'ListCocktail';
  const ListCocktail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: FutureBuilder<List<CocktailModel>>(
        future: fetchCocktails(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return DrinkList(drinks: snapshot.data!);
          } else {
            return const Center(
              // child: CircularProgressIndicator(),
              child: LoadingListPage(),
            );
          }
        },
      ),
    );
  }
}

class DrinkList extends StatelessWidget {
  const DrinkList({Key? key, required this.drinks}) : super(key: key);

  final List<CocktailModel> drinks;
  final _biggerFont = const TextStyle(fontSize: 18.0, color: Colors.white);

  void _pushNamedToScreen(BuildContext context, int index) {
    const routeName = DetailCocktail.routeName;
    Navigator.pushNamed(context, routeName, arguments: drinks[index]);
  }

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: drinks.length,
      itemBuilder: (context, index) => Card(
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        color: Colors.blueAccent,
        child: cellForRowsAt(context, index),
      ),
    );
  }

  Widget cellForRowsAt(BuildContext context, int index) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: ListTile(
        onTap: () {
          _pushNamedToScreen(context, index);
        },
        leading: Text(
          (index + 1).toString(),
          style: _biggerFont,
        ),
        title: Text(
          drinks[index].strDrink ?? '',
          style: _biggerFont,
        ),
        trailing: const Icon(Icons.arrow_forward_ios),
        iconColor: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: _buildListView(context),
    );
  }
}

class LoadingListPage extends StatefulWidget {
  const LoadingListPage({Key? key}) : super(key: key);

  @override
  _LoadingListPageState createState() => _LoadingListPageState();
}

class _LoadingListPageState extends State<LoadingListPage> {
  final bool _enabled = true;

  Widget _loadingCell() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 48.0,
            height: 48.0,
            color: Colors.white,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 8.0,
                  color: Colors.white,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 2.0),
                ),
                Container(
                  width: double.infinity,
                  height: 8.0,
                  color: Colors.white,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 2.0),
                ),
                Container(
                  width: 40.0,
                  height: 8.0,
                  color: Colors.white,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                enabled: _enabled,
                child: ListView.builder(
                  itemCount: 12,
                  itemBuilder: (context, index) => _loadingCell(),
                  // itemBuilder: (_, __) => _loadingCell(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
