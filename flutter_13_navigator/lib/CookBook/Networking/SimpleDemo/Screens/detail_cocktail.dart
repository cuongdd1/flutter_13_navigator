import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/CookBook/Networking/SimpleDemo/Models/cocktail_model.dart';
import 'package:transparent_image/transparent_image.dart';

class DetailCocktail extends StatelessWidget {
  static const routeName = 'DetailCocktail';
  const DetailCocktail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cocktail =
        ModalRoute.of(context)!.settings.arguments as CocktailModel;

    return Scaffold(
      appBar: AppBar(
        title: Text(cocktail.strDrink!),
      ),
      body: Stack(
        children: <Widget>[
          const Center(child: CircularProgressIndicator()),
          Center(
            child: Column(
              children: [
                FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: cocktail.strDrinkThumb ??
                      'https://picsum.photos/250?image=9',
                ),
                Container(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    verticalDirection: VerticalDirection.down,
                    children: [
                      Text(
                        cocktail.strGlass ?? '',
                        style: const TextStyle(fontSize: 24),
                      ),
                      Text(cocktail.strInstructions ?? '',
                          style: const TextStyle(fontSize: 18)),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
