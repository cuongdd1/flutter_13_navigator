import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/CookBook/cookbook_screen.dart';

class Networking extends StatefulWidget {
  static const routeName = 'Networking';
  const Networking({Key? key}) : super(key: key);

  @override
  _NetworkingState createState() => _NetworkingState();
}

class _NetworkingState extends State<Networking> {
  final List<IdentifierScreenModel> _listScreens = [
    const IdentifierScreenModel('List Cocktail', 'ListCocktail'),
    const IdentifierScreenModel('Delete data on the internet', ''),
    const IdentifierScreenModel('Delete data on the internet', ''),
    const IdentifierScreenModel('Fetch data from the internet', ''),
    const IdentifierScreenModel('Make authenticated requests', ''),
    const IdentifierScreenModel('Parse JSON in the background', ''),
    const IdentifierScreenModel('Send data to the internet', ''),
    const IdentifierScreenModel('Update data over the internet', ''),
    const IdentifierScreenModel('Work with WebSockets', ''),
  ];

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _listScreens.length,
      itemBuilder: (context, index) => Card(
        child: tableViewCell(context, index),
      ),
    );
  }

  Widget tableViewCell(BuildContext context, int index) {
    return TextButton(
      onPressed: () {
        _pushNamedToScreen(context, index);
      },
      child: Text(
        _listScreens[index].title,
      ),
    );
  }

  void _pushNamedToScreen(BuildContext context, int index) {
    Navigator.pushNamed(context, _listScreens[index].routeName,
        arguments: _listScreens[index].title);
  }

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
        ),
      ),
      body: _buildListView(context),
    );
  }
}
