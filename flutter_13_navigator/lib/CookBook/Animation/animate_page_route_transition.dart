import 'package:flutter/material.dart';

class AnimateTransitionScreen extends StatelessWidget {
  static const routeName = 'AnimateTransitionScreen';
  const AnimateTransitionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(arguments),
      ),
    );
  }
}
