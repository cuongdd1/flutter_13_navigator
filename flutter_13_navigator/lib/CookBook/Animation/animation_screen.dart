import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/CookBook/cookbook_screen.dart';

import 'animate_page_route_transition.dart';

class AnimationScreen extends StatefulWidget {
  static const routeName = 'AnimationScreen';
  const AnimationScreen({Key? key}) : super(key: key);

  @override
  _AnimationScreenState createState() => _AnimationScreenState();
}

class _AnimationScreenState extends State<AnimationScreen> {
  final List<IdentifierScreenModel> _listScreen = [
    const IdentifierScreenModel(
        'Animate a page route transition', AnimateTransitionScreen.routeName),
    const IdentifierScreenModel('Animate a widget using a physics simulation',
        ''),
    const IdentifierScreenModel('Animate the properties of a container',
        ''),
    const IdentifierScreenModel(
        'Fade a widget in and out', ''),
  ];

  void _pushNamedToScreen(BuildContext context, int index) {
    final String routeName = _listScreen[index].routeName;
    final String title = _listScreen[index].title;
    Navigator.pushNamed(context, routeName, arguments: title);
  }

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _listScreen.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blueAccent,
        child: _cellForRowAt(context, index),
      ),
    );
  }

  Widget _cellForRowAt(BuildContext context, int index) {
    return ListTile(
      title: Text(
        _listScreen[index].title,
        style: const TextStyle(color: Colors.white),
      ),
      onTap: () {
        _pushNamedToScreen(context, index);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Animation'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: _buildListView(context),
      ),
    );
  }
}

extension StringX on String {
  String get firstChar => (this[0]).toLowerCase();
}