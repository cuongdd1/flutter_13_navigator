import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void printMessage(String messgae) {
  if (kDebugMode) {
    print(messgae);
  }
}

class SendDataScreen extends StatefulWidget {
  static const routeName = 'SendDataScreen';
  const SendDataScreen({Key? key}) : super(key: key);

  @override
  _SendDataScreenState createState() => _SendDataScreenState();
}

class _SendDataScreenState extends State<SendDataScreen> {
  String enteredText = '';
  @override
  void didChangeDependencies() {
    printMessage('data received is $enteredText');
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant SendDataScreen oldWidget) {
    printMessage('didUpdateWidget $enteredText');
    super.didUpdateWidget(oldWidget);
  }

  void _navigateAndDisplaySelection(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const ReceivedDataScreen(),
        settings: RouteSettings(arguments: enteredText),
      ),
    );
    
    setState(() {
      printMessage('data callback is $result');
      enteredText = result;
    });

    // After the Selection Screen returns a result, hide any previous snackbars
    // and show the new result.
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
          content: result == '' ? const Text('Khong co gi') : Text('$result')));
  }

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            TextFormField(
              initialValue: enteredText,
              onChanged: (value) => enteredText = value,
            ),
            ElevatedButton(
              onPressed: () => _navigateAndDisplaySelection(context),
              child: const Text('Next'),
            ),
            Text(enteredText),
            ElevatedButton(
              onPressed: () => setState(() {
                
              }),
              child: const Text('Set State'),
            ),
          ],
        ),
      ),
    );
  }
}

class ReceivedDataScreen extends StatelessWidget {
  const ReceivedDataScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String data = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8),
                child: TextFormField(
                  onChanged: (value) => data = value,
                  initialValue: data,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context, data);
                  },
                  child: const Text('Save'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
