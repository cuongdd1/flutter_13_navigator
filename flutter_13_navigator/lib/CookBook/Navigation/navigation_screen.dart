import 'package:flutter/material.dart';
import 'package:flutter_13_navigator/CookBook/cookbook_screen.dart';

class NavigationScreen extends StatefulWidget {
  static const routeName = 'NavigationScreen';
  const NavigationScreen({Key? key}) : super(key: key);

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  final List<IdentifierScreenModel> _listScreen = [
    const IdentifierScreenModel('Animate a widget across screens', ''),
    const IdentifierScreenModel('Navigate to a new screen and back', ''),
    const IdentifierScreenModel('Navigate with named routes', ''),
    const IdentifierScreenModel('Pass arguments to a named route', ''),
    const IdentifierScreenModel('Return data from a screen', 'HomeDataScreen'),
    const IdentifierScreenModel('Send data to a new screen', 'SendDataScreen'),
  ];

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _listScreen.length,
      itemBuilder: (context, index) => Card(
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        color: Colors.blueAccent,
        child: _buildCell(context, index),
      ),
    );
  }

  void _pushNamedToScreen(BuildContext context, int index) {
    Navigator.pushNamed(context, _listScreen[index].routeName,
        arguments: _listScreen[index].title);
  }

  Widget _buildCell(BuildContext context, int index) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: ListTile(
        onTap: () {
          _pushNamedToScreen(context, index);
        },
        title: Text(
          _listScreen[index].title,
          style: const TextStyle(color: Colors.white, fontSize: 18),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: _buildListView(context),
      ),
    );
  }
}
