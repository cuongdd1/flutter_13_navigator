import 'package:flutter/material.dart';

class NullScreen extends StatelessWidget {
  static const routeName = '';
  const NullScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as String;

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: const Center(
        child: Text('Nội dung này sắp được học'),
      ),
    );
  }
}
