import 'package:flutter/material.dart';

class CookBookScreen extends StatefulWidget {
  const CookBookScreen({Key? key}) : super(key: key);
  static const routeName = 'CookBookScreen';

  @override
  _CookBookScreenState createState() => _CookBookScreenState();
}

class _CookBookScreenState extends State<CookBookScreen> {
  final List<IdentifierScreenModel> _listComponents = [
    const IdentifierScreenModel('Animation', 'AnimationScreen'),
    const IdentifierScreenModel('Design', ''),
    const IdentifierScreenModel('Effects', 'Effects'),
    const IdentifierScreenModel('Forms', ''),
    const IdentifierScreenModel('Gestures', ''),
    const IdentifierScreenModel('Images', 'ImagesScreen'),
    const IdentifierScreenModel('Lists', 'ListsScreen'),
    const IdentifierScreenModel('Maintenance', ''),
    const IdentifierScreenModel('Navigation', 'NavigationScreen'),
    const IdentifierScreenModel('Networking', 'Networking'),
    const IdentifierScreenModel('Persistence', ''),
    const IdentifierScreenModel('Plugins', ''),
    const IdentifierScreenModel('Testing', ''),

    // const IdentifierScreenModel('Horizontal List', 'Horizontal List'),
    // const IdentifierScreenModel('Grid List', 'Grid List'),
  ];
  final _biggerFont = const TextStyle(fontSize: 18.0, color: Colors.white);

  void _pushNamedToScreen(BuildContext context, int index) {
    final routeName = _listComponents[index].routeName;
    final title = _listComponents[index].title;
    Navigator.pushNamed(context, routeName, arguments: title);
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      itemCount: _listComponents.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blueAccent,
        shadowColor: Colors.blueGrey,
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        child: cellForRowsAt(context, index),
      ),
    );
  }

  Widget cellForRowsAt(BuildContext context, int index) {
    return ListTile(
      onTap: () {
        _pushNamedToScreen(context, index);
      },
      leading: Text(
        (index + 1).toString(),
        style: _biggerFont,
      ),
      title: Text(
        _listComponents[index].title,
        style: _biggerFont,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            arguments,
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: _buildSuggestions(),
        ));
  }
}

class IdentifierScreenModel {
  final String title;
  final String routeName;

  const IdentifierScreenModel(this.title, this.routeName);
}
